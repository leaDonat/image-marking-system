﻿using DalContracs.DTO;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Request.DocumentShares;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceImplValidation
{
    static public class DocumentMarkerValidation
    {
        static public Boolean checkDocumentMarkerRequest(DocumentMarkerRequest docMarker)
        {
            if (docMarker.backColor != null && docMarker.docID != null && docMarker.foreColor != null && docMarker.markerLocationX != 0 && docMarker.markerLocationY != 0 && docMarker.userID != null)
                return true;
            return false;
        }
        static public Boolean checkDocumentMarkerIDRequest(DocumentMarkerIDRequest markerID)
        {
            if (markerID != null)
                return true;
            return false;
        }
        static public Boolean checkDocumentMarkerDetails(DocumentMarkerDetails docMarker)
        {
            if (docMarker != null&& docMarker.backColor != null && docMarker.docID != null && docMarker.foreColor != null && docMarker.markerLocationX != 0 && docMarker.markerLocationY != 0 && docMarker.userID != null)
                return true;
            return false;
        }
    }
}
