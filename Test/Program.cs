﻿using DalContracs;
using InfraDALContracts;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using FactoryDI;
using System;
using System.IO;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "dll");
            var resolver = new Resolver(path);
            IUserService user = resolver.Resolve<IUserService>();
            user.CreateUSer(new User() { userID = "eee", userName = "eee" });
            IUserService user1 = resolver.Resolve<IUserService>();
            user1.CreateUSer(new User() { userID = "ddd", userName = "ddd" });
            Console.WriteLine(user.GetUser("bbb"));
            Console.ReadLine();
        }
    }
}
