﻿using DalContracs.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.WebSockets;
using DalContracs.Request.DocumentShares;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Respones.share;
using DalContracs.Respones.webSocket;
using DalContracs.Request.Document;

namespace WebSocketShare
{
    public class ShareWebSocketAdapter : IshareWebSocket
    {
        Dictionary<Guid, Dictionary<string, WebSocket>> _sockets;
        ISharedDocumentService service;
        readonly ILogger<ShareWebSocketAdapter> _logger;
        public ShareWebSocketAdapter(ILogger<ShareWebSocketAdapter> logger, ISharedDocumentService _service)
        {
            _logger = logger;
            _sockets = new Dictionary<Guid, Dictionary<string, WebSocket>>();
            service = _service;
        }
        public async Task<IReceiver> Add(DocumentShare share, ISocket socket)
        {
            var webSocketAdapter = socket as WebSocketAdapter;
            Receiver retval = null;
            if (service.GetShare(new DocumentShareRequest { userID = share.userID, docID = share.docID }).responseType.Equals("GetShareResponseOK"))
            {
                if (_sockets.ContainsKey(share.docID))
                {
                    if (_sockets[share.docID].ContainsKey(share.userID))
                        _sockets[share.docID].Remove(share.userID);
                }
                else
                    _sockets.Add(share.docID, new Dictionary<string, WebSocket>());
                _sockets[share.docID].Add(share.userID, webSocketAdapter.Socket);
                retval = new Receiver(webSocketAdapter.Socket);
            }
            return retval;
        }
        public async Task CheckShare(webSocketResponse doc)
        {
            if (_sockets.ContainsKey(doc.marker.docID))
                foreach (var item in _sockets[doc.marker.docID].Keys)
                {
                    if (item != doc.marker.userID)
                    {
                        var message = JsonConvert.SerializeObject(doc);
                        var buffer = Encoding.UTF8.GetBytes(message);
                        await _sockets[doc.marker.docID][item].SendAsync(new ReadOnlyMemory<byte>(buffer), WebSocketMessageType.Text
                            , true
                           , CancellationToken.None);
                    }
                }
        }
        public void CloseWebSocket(DocumentShareRequest share)
        {
            if (_sockets.ContainsKey(share.docID) && _sockets[share.docID].ContainsKey(share.userID))
                _sockets[share.docID].Remove(share.userID);
        }
    }
}

