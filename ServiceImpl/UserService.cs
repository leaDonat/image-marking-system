﻿using FactoryContracts;
using System;
using DalContracs.InterfaceService;
using DalContracs.DTO;
using System.Collections.Generic;
using DalContracs;
using DalContracs.Request.Users;
using DalContracs.Respones.user;

namespace ServiceImpl
{
    [Register(Policy.Transient, typeof(IUserService))]
    public class UserService : IUserService
    {
        IUserDal userDal;
        public UserService(IUserDal _userDal)
        {
            userDal = _userDal;
        }

        public UserResponse CreateUSer(UserRequest user)
        {
            if (ServiceImplValidation.UserValidation.checkUserRequest(user))
                return userDal.CreateUser(user);
            return new UserNotExistsResponse();
        }


        public UserResponse GetUser(UserIDRequest userID)
        {
            if (ServiceImplValidation.UserValidation.checkUserIDRequest(userID))
            {
                return userDal.GetUser(userID);
            }
            return new UserNotExistsResponse();
        }
        public UserResponse GetUsers()
        {
            return userDal.GetUsers();
        }

        public UserResponse Login(UserRequest user)
        {
            if (ServiceImplValidation.UserValidation.checkUserRequest(user))
            {
                return userDal.Login(user);
            }
            return new propretyUserNullRespones();
        }

        public UserResponse RemoveUser(UserIDRequest user)
        {
            if (ServiceImplValidation.UserValidation.checkUserIDRequest(user))
            {
                return userDal.RemoveUser(user);
            }
            return new propretyUserNullRespones();
        }


    }
}
