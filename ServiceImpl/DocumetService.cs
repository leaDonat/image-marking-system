﻿using FactoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using DalContracs.InterfaceService;
using System.Reflection.Metadata;
using DalContracs;
using DalContracs.DTO;
using DalContracs.Request;
using DalContracs.Request.Document;
using DalContracs.Respones.document;
using DalContracs.Request.Users;

namespace ServiceImpl
{
    [Register(Policy.Transient, typeof(IDocumentService))]
    public class DocumetService : IDocumentService
    {
        IDocumentDAl docDal;
        public DocumetService(IDocumentDAl _docDal)
        {
            docDal = _docDal;
        }
        public DocumentResponse CreateDocument(DocumentRequest doc)
        {
            if (ServiceImplValidation.DocumetServiceValidation.checkDocumentRequest(doc))
            {
                   return docDal.CreateDocument(doc);
            }
            return new PropertyDocumentNull();
        }
        public DocumentResponse CreateCopyFile(DocumentIDRequest docID)
        {
            if (ServiceImplValidation.DocumetServiceValidation.checkDocumentIDRequest(docID))
            {
                   return docDal.CreateCopyFile(docID);
            }
            return new PropertyDocumentNull();
        }

        public DocumentResponse GetDocument(DocumentIDRequest doc)
        {
            if (ServiceImplValidation.DocumetServiceValidation.checkDocumentIDRequest(doc))
            {
                return docDal.GetDocument(doc);
            }
            return new PropertyDocumentNull();
        }

        public DocumentResponse GetDocumentsForUser(UserIDRequest userID)
        {
            if (ServiceImplValidation.UserValidation.checkUserIDRequest(userID))
                return docDal.GetDocumentsForUser(userID);
            return new PropertyDocumentNull();
        }


            public DocumentResponse GetDocuments()
        {
            return docDal.GetDocuments();
        }

        public DocumentResponse RemoveDocument(DocumentIDRequest doc)
        {
            if (ServiceImplValidation.DocumetServiceValidation.checkDocumentIDRequest(doc))
            {
                if (GetDocument(doc) != null)
                    return docDal.RemoveDocument(doc); 
                return new DocumentNotExists();
            }
            return new PropertyDocumentNull();
        }
    }
}
