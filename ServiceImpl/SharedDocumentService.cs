﻿using DalContracs;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentShares;
using DalContracs.Request.Users;
using DalContracs.Respones.share;
using DalContracs.Respones.user;
using DBParameterConverter;
using FactoryContracts;
using InfraDALContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ServiceImpl
{
    [Register(Policy.Transient, typeof(ISharedDocumentService))]
    public class SharedDocumentService : ISharedDocumentService
    {
        ISharedDocumentDal shardDal;
        public SharedDocumentService(ISharedDocumentDal _shardDal)
        {
            shardDal = _shardDal;
        }
        public ShareResponse CreateShare(DocumentShareRequest docShare)
        {
           if(ServiceImplValidation.SharedDocumentValidation.checkDocumentShareRequest(docShare))
            {
                return shardDal.CreateShare(docShare);
            }
            return new PropertyShareNullResponse();
        }

        public ShareResponse GetShare(DocumentShareRequest doc)
        {
            if (ServiceImplValidation.SharedDocumentValidation.checkDocumentShareRequest(doc))
            {
                return shardDal.GetShare(doc);
            }
            return new PropertyShareNullResponse(); 
        }
        public ShareResponse GetShares(DocumentShareRequest doc)
        {
            if (ServiceImplValidation.SharedDocumentValidation.checkDocumentShareRequest(doc))
            {
                return shardDal.GetShares(doc);
            }
            return new PropertyShareNullResponse();
        }
        public ShareResponse RemoveShare(DocumentShareRequest doc)
        {
            if (ServiceImplValidation.SharedDocumentValidation.checkDocumentShareRequest(doc))
            {
                if (GetShare(doc) != null)
                    return shardDal.RemoveShare(doc);
                return new ShareNotExists();
            }
            return new PropertyShareNullResponse();
        }
        public ShareResponse GetUsersOfShare(DocumentIDRequest docID)
        {
            if (ServiceImplValidation.DocumetServiceValidation.checkDocumentIDRequest(docID))
            {
                return shardDal.GetUsersOfShare(docID);
            }
            return new PropertyShareNullResponse();
        }
    }
}
