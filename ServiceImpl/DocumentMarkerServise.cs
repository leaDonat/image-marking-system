﻿using DalContracs;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Respones.marker;
using DalContracs.Respones.webSocket;
using DalContracs.WebSocket;
using FactoryContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceImpl
{
    [Register(Policy.Transient, typeof(IDocumentMarkerServise))]
    public class DocumentMarkerServise : IDocumentMarkerServise
    {
        IDocumentMarkerDAl markerDal;
        IshareWebSocket sockt;
        public DocumentMarkerServise(IDocumentMarkerDAl _markerDAl, IshareWebSocket _sockt)
        {
            markerDal = _markerDAl;
            sockt = _sockt;
        }
        public MarkerResponse CreateMarker(DocumentMarkerRequest docMarker)
        {
            if (ServiceImplValidation.DocumentMarkerValidation.checkDocumentMarkerRequest(docMarker))
            {
                MarkerResponse marker = markerDal.CreateMarker(docMarker);
                if (marker.responseType.Equals("CreateMarkerResponseOK"))
                {
                    sockt.CheckShare(new webSocketResponse { marker = marker.marker, responseType = "CreateMarker" });
                    return marker;
                }
            }
            return new propertyMarkerNull();
        }

        public MarkerResponse GetMarker(DocumentMarkerIDRequest marker)
        {
            if (ServiceImplValidation.DocumentMarkerValidation.checkDocumentMarkerIDRequest(marker))
            {
                return markerDal.GetMarker(marker);
            }
            return new propertyMarkerNull();
        }

        public MarkerResponse GetMarkers(DocumentIDRequest docID)
        {
            if(ServiceImplValidation.DocumetServiceValidation.checkDocumentIDRequest(docID))
            return markerDal.GetMarkers(docID);
            return new propertyMarkerNull();
        }

        public MarkerResponse RemoveMarker(DocumentMarkerDetails docMarker)
        {
            MarkerResponse marker = markerDal.RemoveMarker(new DocumentMarkerIDRequest { DocMarkerID = docMarker.markerID });
            if (ServiceImplValidation.DocumentMarkerValidation.checkDocumentMarkerDetails(docMarker))
            {
                sockt.CheckShare(new webSocketResponse { marker =docMarker, responseType = "RemoveMarker" });
                return marker;
            }
            return new propertyMarkerNull();
        }
        public MarkerResponse changeMarker(DocumentMarkerDetails docMarker)
        {
            MarkerResponse marker = markerDal.changeMarker(docMarker);
            if (ServiceImplValidation.DocumentMarkerValidation.checkDocumentMarkerDetails(docMarker))
            {
                sockt.CheckShare(new webSocketResponse { marker = docMarker, responseType = "changeMarker" });
                return marker;
            }
            return new propertyMarkerNull();
        }
    }
}
