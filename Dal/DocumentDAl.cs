﻿using System.Collections.Generic;
using DalContracs;
using DalContracs.DTO;
using InfraDALContracts;
using System.Data.SqlClient;
using System.Data;
using FactoryContracts;
using DBParameterConverter;
using DalContracs.Request.Document;
using DalContracs.Respones.document;
using DalContracs.Request.Users;
using Microsoft.Extensions.Configuration;
using System;

namespace Dal
{
    [Register(Policy.Transient, typeof(IDocumentDAl))]
    public class DocumentDAl : IDocumentDAl
    {
        IDBConnection conect;
        IInfraDAL SqlDal;
        IConvertParameter convert;
        IConfiguration _conf;
        DBParameterConverterClass parameterConver;

        public DocumentDAl(IInfraDAL sqlDal, IConvertParameter _convert, IConfiguration conf)
        {
            _conf = conf;
            SqlDal = sqlDal;
            convert = _convert;
            var name = _conf.GetSection("AppDB:connectionString");
            conect = SqlDal.Connect(name.Value);
            parameterConver = new DBParameterConverterClass(SqlDal);
        }
        public DocumentResponse CreateDocument(DocumentRequest doc)
        {
            try
            {
                DataSet docSql = SqlDal.ExesSQuery("CreateDocument", conect, convert.ConvertToListParametr<DocumentRequest>(doc));
                if (docSql.Tables[0].Rows.Count > 0)
                {
                    return new CreateDocumentResponseOK { ImgDoc = convert.ConvertToResponse<ImgDocumentDetails>(docSql) };
                }
                return new UserNotExsist();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public DocumentResponse CreateCopyFile(DocumentIDRequest docID)
        {
            try
            {
                DataSet docSql = SqlDal.ExesSQuery("CreateCopyFile", conect, convert.ConvertToListParametr<DocumentIDRequest>(docID));
                if (docSql.Tables[0].Rows.Count > 0)
                {
                    return new CopyDocResponseOK { docID = convert.ConvertToResponse<Guid>(docSql) };
                }
                return new DocumentNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public DocumentResponse GetDocument(DocumentIDRequest doc)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetDocument", conect, convert.ConvertToListParametr<DocumentIDRequest>(doc));
                return new GetDocumentResponseOK { ImgDoc = convert.ConvertToResponse<ImgDocumentDetails>(ds) };
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public DocumentResponse GetDocuments()
        {
            try
            {
                var docs = new List<DocumentResponse>();
                DataSet ds = SqlDal.ExesSQuery("GetDocuments", conect);
                return new GetDocumentsResponseOK { ImgsDoc = convert.ConvertToLisResponse<ImgDocumentDetails>(ds) };
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public DocumentResponse RemoveDocument(DocumentIDRequest docID)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("RemoveDocument", conect, convert.ConvertToListParametr<DocumentIDRequest>(docID));
                if (ds > 0)
                {
                    return new RemoveDocumentResponseOK();
                }
                return new DocumentNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public DocumentResponse GetDocumentsForUser(UserIDRequest userID)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetDocumentsForUser", conect, convert.ConvertToListParametr<UserIDRequest>(userID));
                return new GetAllDocumentsUserResponseOK { ImgsDoc = convert.ConvertToLisResponse<ImgDocumentDetails>(ds) };
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
    }
}