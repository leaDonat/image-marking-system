﻿using DalContracs;
using FactoryContracts;
using InfraDALContracts;
using DalContracs.DTO;
using System.Data;
using DBParameterConverter;
using DalContracs.Request.DocumentShares;
using DalContracs.Respones.share;
using DalContracs.Request.Document;
using DalContracs.Request.Users;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Dal
{
    [Register(Policy.Transient, typeof(ISharedDocumentDal))]
    public class SharedDocumentDal : ISharedDocumentDal
    {
        IDBConnection conect;
        DBParameterConverterClass parameterConver;
        IInfraDAL SqlDal;
        IConvertParameter convert;
        IConfiguration _conf;
        public SharedDocumentDal(IInfraDAL _sqlDal, IConvertParameter _convert, IConfiguration conf)
        {
            _conf = conf;
            SqlDal = _sqlDal;
            convert = _convert;
            var name = _conf.GetSection("AppDB:connectionString");
            conect = SqlDal.Connect(name.Value);
            parameterConver = new DBParameterConverterClass(SqlDal);
        }
        public ShareResponse CreateShare(DocumentShareRequest share)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("CreateShare", conect, convert.ConvertToListParametr<DocumentShareRequest>(share));
                if (ds >= 1)
                    return new CreateShareResponseOK();
                return new ShareNotExists();
            }
            catch (SqlException e)
            {
                return new DalContracs.Respones.share.sqlException(e.Message);
            }
        }


        public ShareResponse GetShare(DocumentShareRequest doc)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetShare", conect, convert.ConvertToListParametr<DocumentShareRequest>(doc));
                if (ds.Tables[0].Rows.Count == 0)
                    return new ShareNotExists();
                return new GetShareResponseOK { docShare = convert.ConvertToResponse<DocumentShare>(ds) };
            }
            catch (SqlException e)
            {
                return new DalContracs.Respones.share.sqlException(e.Message);
            }
        }
        public ShareResponse GetShares(DocumentShareRequest doc)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetShares", conect, convert.ConvertToListParametr<DocumentShareRequest>(doc));
                if (ds.Tables[0].Rows.Count > 0)
                    return new GetShareResponseOK { docShares = convert.ConvertToLisResponse<DocumentShare>(ds) };
                return new ShareNotExists();
            }
            catch (SqlException e)
            {
                return new DalContracs.Respones.share.sqlException(e.Message);
            }
        }

        public ShareResponse RemoveShare(DocumentShareRequest doc)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("RemoveShare", conect, convert.ConvertToListParametr<DocumentShareRequest>(doc));
                if (ds > 0)
                {
                    return new RemoveShareResponseOK();
                }
                return new ShareNotExists();
            }
            catch (SqlException e)
            {
                return new DalContracs.Respones.share.sqlException(e.Message);
            }
        }
        public ShareResponse GetUsersOfShare(DocumentIDRequest docID)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetUsersOfShare", conect, convert.ConvertToListParametr<DocumentIDRequest>(docID));
                return new GetUsersOfShareResponseOK { listUserOfShare = convert.ConvertToLisResponse<UserIDRequest>(ds) };
            }
            catch (SqlException e)
            {
                return new DalContracs.Respones.share.sqlException(e.Message);
            }
        }
    }
}
