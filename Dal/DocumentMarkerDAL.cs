﻿using DalContracs;
using DalContracs.DTO;
using System.Data.SqlClient;
using System.Data;
using FactoryContracts;
using InfraDALContracts;
using DBParameterConverter;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Request.Document;
using DalContracs.Respones.marker;
using Microsoft.Extensions.Configuration;

namespace Dal
{
    [Register(Policy.Transient, typeof(IDocumentMarkerDAl))]
    public class DocumentMarkerDAL : IDocumentMarkerDAl
    {

        IDBConnection conect;
        IInfraDAL SqlDal;
        IConvertParameter convert;
        DBParameterConverterClass parameterConver;
        IConfiguration _conf;
        public DocumentMarkerDAL(IInfraDAL sqlDal, IConvertParameter _convert, IConfiguration conf)
        {
            _conf = conf;
            SqlDal = sqlDal;
            convert = _convert;
            var name = _conf.GetSection("AppDB:connectionString");
            conect = SqlDal.Connect(name.Value);
            parameterConver = new DBParameterConverterClass(SqlDal);
        }


        public MarkerResponse CreateMarker(DocumentMarkerRequest marker)
        {
            try
            {
                DataSet docSql = SqlDal.ExesSQuery("CreateMarker", conect, convert.ConvertToListParametr<DocumentMarkerRequest>(marker));
                if (docSql.Tables.Count > 0)
                {
                    DataRow dr = docSql.Tables[0].Rows[0];
                    return new CreateMarkerResponseOK { marker = convert.ConvertToResponse<DocumentMarkerDetails>(docSql) };
                }
                return new MarkerNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public MarkerResponse GetMarker(DocumentMarkerIDRequest marker)
        {
            try
            {
                var marke = new DocumentMarker();
                DataSet ds = SqlDal.ExesSQuery("GetMarker", conect, convert.ConvertToListParametr<DocumentMarkerIDRequest>(marker));
                if (ds.Tables[0].Rows.Count > 0)
                    return new GetMarkerResponse { marker = convert.ConvertToResponse<DocumentMarkerDetails>(ds) };
                return new MarkerNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public MarkerResponse GetMarkers(DocumentIDRequest docID)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetMarkers", conect, convert.ConvertToListParametr<DocumentIDRequest>(docID));
                if (ds.Tables[0].Rows.Count > 0)
                    return new GetMarkerResponse { markers = convert.ConvertToLisResponse<DocumentMarkerDetails>(ds) };
                return new MarkerNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public MarkerResponse RemoveMarker(DocumentMarkerIDRequest marker)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("RemoveMarker", conect, convert.ConvertToListParametr<DocumentMarkerIDRequest>(marker));
                if (ds > 0)
                {
                    return new RemoveMarkerResponseOK();
                }
                return new MarkerNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public MarkerResponse changeMarker(DocumentMarkerDetails marker)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("changeMarker", conect, convert.ConvertToListParametr<DocumentMarkerDetails>(marker));
                if (ds > 0)
                {
                    return new changeMarkerResponseOK();
                }
                return new MarkerNotExists();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

    }
}
