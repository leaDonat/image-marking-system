﻿using DalContracs.DTO;
using InfraDALContracts;
using System.Data.SqlClient;
using System.Data;
using FactoryContracts;
using DalContracs;
using DBParameterConverter;
using DalContracs.Request.Users;
using DalContracs.Respones.user;
using Microsoft.Extensions.Configuration;

namespace Dal
{
    [Register(Policy.Transient, typeof(IUserDal))]
    public class UserDAL : IUserDal
    {
        IDBConnection conect;
        DBParameterConverterClass parameterConver;
        IInfraDAL SqlDal;
        IConvertParameter convert;
        IConfiguration _conf;
        // עכשיו תראי DB
        public UserDAL(IInfraDAL sqlDal, IConvertParameter _convert, IConfiguration conf)
        {
            _conf = conf;
            convert = _convert;
            SqlDal = sqlDal;
            var name = _conf.GetSection("AppDB:connectionString");
            conect = SqlDal.Connect(name.Value);
            parameterConver = new DBParameterConverterClass(sqlDal);
        }
        public UserResponse CreateUser(UserRequest user)
        {
            try
            {
                if (GetUser(new UserIDRequest { UserID = user.userID }).responseType.Equals("UserNotExistsResponse"))
                {
                    SqlDal.ExesSQuery("CreateUser", conect, parameterConver.ConvertToParameters2<UserRequest>(user));
                    return new RegisterUserResponseOK();
                }
                return new UserExistsResponse();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public UserResponse Login(UserRequest user)
        {
            try
            {
                DataSet intds = SqlDal.ExesSQuery("Login", conect, parameterConver.ConvertToParameters2<UserRequest>(user));
                if ((int)intds.Tables[0].Rows[0].ItemArray[0] > 0)
                {
                    DataSet ds = SqlDal.ExesSQuery("GetDocumentsForUser", conect, convert.ConvertToListParametr<UserIDRequest>(new UserIDRequest { UserID = user.userID }));
                    return new LoginUserResponseOK { listDocuments = convert.ConvertToLisResponse<ImgDocumentDetails>(ds) };
                }
                return new UserNotExistsResponse();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public UserResponse RemoveUser(UserIDRequest userID)
        {
            try
            {
                int ds = SqlDal.ExecSPQueryInt("MarkUserRemoved", conect, convert.ConvertToListParametr<UserIDRequest>(userID));
                if (ds > 0)
                    return new RemoveUserResponseOK();
                return new UserNotExistsResponse();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }

        public UserResponse GetUser(UserIDRequest user)
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetUser", conect, convert.ConvertToListParametr<UserIDRequest>(user));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    return new GetUserResponse { user = convert.ConvertToResponse<User>(ds) };
                }
                return new UserNotExistsResponse();
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
        public UserResponse GetUsers()
        {
            try
            {
                DataSet ds = SqlDal.ExesSQuery("GetUsers", conect);
                return new GetUsersResponse { Users = convert.ConvertToLisResponse<User>(ds) };
            }
            catch (SqlException e)
            {
                return new sqlException(e.Message);
            }
        }
    }
}
