﻿using DalContracs.DTO;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Respones.marker;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs
{
    public interface IDocumentMarkerDAl
    {
        MarkerResponse CreateMarker(DocumentMarkerRequest marker);
        MarkerResponse RemoveMarker(DocumentMarkerIDRequest marker);
        MarkerResponse GetMarker(DocumentMarkerIDRequest marker);
        MarkerResponse GetMarkers(DocumentIDRequest docID);
        MarkerResponse changeMarker(DocumentMarkerDetails marker);
    }
}
