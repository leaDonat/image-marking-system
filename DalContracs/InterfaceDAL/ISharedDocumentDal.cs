﻿using DalContracs.DTO;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentShares;
using DalContracs.Respones.share;
using DalContracs.Respones.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs
{
    public interface ISharedDocumentDal
    {
        ShareResponse CreateShare(DocumentShareRequest share);
        ShareResponse RemoveShare(DocumentShareRequest share);
        ShareResponse GetShare(DocumentShareRequest doc);
        ShareResponse GetUsersOfShare(DocumentIDRequest docID);
        ShareResponse GetShares(DocumentShareRequest doc);

    }
}
