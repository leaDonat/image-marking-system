﻿using DalContracs.DTO;
using DalContracs.Request.Users;
using DalContracs.Respones.user;
using System;
using System.Collections.Generic;

namespace DalContracs
{
    public interface IUserDal
    {
        UserResponse Login(UserRequest user);
        UserResponse CreateUser(UserRequest user);
        UserResponse RemoveUser(UserIDRequest userID);
        UserResponse GetUser(UserIDRequest userID);
        UserResponse GetUsers();

    }
}
