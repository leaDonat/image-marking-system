﻿using DalContracs.DTO;
using DalContracs.Request;
using DalContracs.Request.Document;
using DalContracs.Request.Users;
using DalContracs.Respones.document;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs
{
   public interface IDocumentDAl
    {
        DocumentResponse CreateDocument(DocumentRequest doc);
        DocumentResponse CreateCopyFile(DocumentIDRequest docID);
        DocumentResponse GetDocument(DocumentIDRequest doc);
        DocumentResponse GetDocuments();
        DocumentResponse RemoveDocument(DocumentIDRequest doc);
        DocumentResponse GetDocumentsForUser(UserIDRequest userID);

    }
}
