﻿using DalContracs.DTO;
using DalContracs.Request.DocumentShares;
using DalContracs.Respones.share;
using DalContracs.Respones.webSocket;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DalContracs.WebSocket
{
   public interface IshareWebSocket
    {
         Task<IReceiver> Add(DocumentShare docID, ISocket socket);
        Task CheckShare(webSocketResponse doc);
        void CloseWebSocket(DocumentShareRequest share);
    }
}
