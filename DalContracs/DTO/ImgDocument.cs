﻿using DBParameterConverter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.DTO
{

    public class ImgDocument
    {
        public string userID { set; get; }

        public string imageUrl { set; get; }

        public string documentName { set; get; }

    }
}
