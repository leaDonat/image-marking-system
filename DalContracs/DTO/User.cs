﻿using DBParameterConverter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.DTO
{
    public class User
    {
        [DBParameter("userID")]
        public string userID { set; get; }

        [DBParameter("userName")]
        public string userName { set; get; }
    }
}
