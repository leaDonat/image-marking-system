﻿using DBParameterConverter;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.DTO
{
    public class DocumentShare
    {
        public Guid docID { set; get; }

        public string userID { set; get; }

        public int statuse { get; set; }

    }
}
