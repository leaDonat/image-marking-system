﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.DTO
{
    public class DocumentMarker
    {
       public Guid docID { set; get; }
        public string markerType { set; get; }
        public int markerLocationX { set; get; }
        public int markerLocationY { set; get; }
        public int radiusX { set; get; }
        public int radiusY { set; get; }
        public string foreColor { set; get; }
        public string backColor { set; get; }
        public string userID { set; get; }
        public string myText { set; get; }
    }
}
