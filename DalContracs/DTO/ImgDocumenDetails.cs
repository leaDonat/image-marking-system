﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.DTO
{
    public class ImgDocumentDetails:ImgDocument
    {
        public Guid docID { get; set; }
        public string dataString { set; get; }
        public int statuse { get; set; }
    }
}
