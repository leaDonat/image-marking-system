﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.Request.DocumentMarkers
{
    public class DocumentMarkerIDRequest
    {
        public Guid DocMarkerID { set; get; }
    }
}
