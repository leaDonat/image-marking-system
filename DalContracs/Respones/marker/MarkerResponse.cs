﻿using DalContracs.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.Respones.marker
{
    public class MarkerResponse
    {
        public string responseType { get; }
        public Guid markerID { get; set; }
        public DocumentMarkerDetails marker { set; get; }
        public List<DocumentMarkerDetails> markers { set; get; }
        public MarkerResponse()
        {
            responseType = this.GetType().Name;
            markers = new List<DocumentMarkerDetails>();
        }
    }
    public class CreateMarkerResponseOK : MarkerResponse
    {
    }
    public class RemoveMarkerResponseOK : MarkerResponse
    {
    }
    public class GetMarkerResponse : MarkerResponse
    {
    }
    public class GetMarkersResponse : MarkerResponse
    {
    }

    public class propertyMarkerNull : MarkerResponse
    {
    }
    public class MarkerNotExists : MarkerResponse
    {
    }
    public class changeMarkerResponseOK : MarkerResponse
    {
    }
    public class sqlException : MarkerResponse
    {
        public string eror;
        public sqlException(string e)
        {
            eror = e;
        }
    }
}
