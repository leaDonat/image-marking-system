﻿using DalContracs.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.Respones.user
{
    public class UserResponse
    {
        public string responseType { get; set; }
        public User user { get; set; }
        public List<ImgDocumentDetails> listDocuments { set; get; }
        public UserResponse()
        {
            listDocuments = new List<ImgDocumentDetails>();
        }

    }
    public class LoginUserResponseOK : UserResponse
    {
        public LoginUserResponseOK()
        {
            responseType = this.GetType().Name;
        }
    } 
    public class RegisterUserResponseOK : UserResponse
    {
        public RegisterUserResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class UserNotExistsResponse : UserResponse
    {
        public UserNotExistsResponse()
        {
            responseType = this.GetType().Name;
        }
    }
    public class RemoveUserResponseOK : UserResponse
    {
        public RemoveUserResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class propretyUserNullRespones : UserResponse
    {
        public propretyUserNullRespones()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetUserResponse : UserResponse
    {
        public GetUserResponse()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetUsersResponse : UserResponse
    {
        public List<User> Users { get; set; }
        public GetUsersResponse()
        {
            responseType = this.GetType().Name;
        }
    }
    
    public class UserExistsResponse : UserResponse
    {
        public UserExistsResponse()
        {
            responseType = this.GetType().Name;
        }
    }
    public class sqlException : UserResponse
    {
        public string eror;
        public sqlException(string e)
        {
            responseType = this.GetType().Name;
            eror = e;
        }
    }
}
