﻿using DalContracs.DTO;
using DalContracs.Respones.document;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.Respones.document
{
    public class DocumentResponse
    {
        public string responseType { get; set; }
        public ImgDocumentDetails ImgDoc { set; get; }
        public List<ImgDocumentDetails> ImgsDoc { set; get; }
        public Guid docID { set; get; }



        public DocumentResponse()
        {
            ImgDoc = new ImgDocumentDetails();
            ImgsDoc = new List<ImgDocumentDetails>();
        }

    }
    public class CreateDocumentResponseOK : DocumentResponse
    {
        public CreateDocumentResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class CopyDocResponseOK : DocumentResponse
    {
        public CopyDocResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetDocumentResponseOK : DocumentResponse
    {
        public GetDocumentResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetDocumentsResponseOK : DocumentResponse
    {
        public GetDocumentsResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetAllDocumentsUserResponseOK : DocumentResponse
    {
        public GetAllDocumentsUserResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class RemoveDocumentResponseOK : DocumentResponse
    {
        public RemoveDocumentResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class PropertyDocumentNull : DocumentResponse
    {
        public PropertyDocumentNull()
        {
            responseType = this.GetType().Name;
        }

    }
    public class DocumentNotExists : DocumentResponse
    {
        public DocumentNotExists()
        {
            responseType = this.GetType().Name;
        }
    }
   
    
    public class UserNotExsist: DocumentResponse
    {
        public UserNotExsist()
        {
            responseType = this.GetType().Name;
        }
    }
    public class sqlException : DocumentResponse
    {
        public string eror;
        public sqlException(string e)
        {
            responseType = this.GetType().Name;
                eror = e;
        }
    }

}
