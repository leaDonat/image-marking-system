﻿using DalContracs.DTO;
using DalContracs.Request.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.Respones.share
{
    public class ShareResponse
    {
        public string responseType { get; set; }
        public DocumentShare docShare { set; get; }
        public List<DocumentShare> docShares { set; get; }
        public List<UserIDRequest> listUserOfShare { set; get; }
        public ShareResponse()
        {
            listUserOfShare = new List<UserIDRequest>();
            docShares = new List<DocumentShare>();
        }
    }
    public class CreateShareResponseOK : ShareResponse
    {
        public CreateShareResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetShareResponseOK : ShareResponse
    {
        public GetShareResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class GetUsersOfShareResponseOK : ShareResponse
    {
        public GetUsersOfShareResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class RemoveShareResponseOK : ShareResponse
    {
        public RemoveShareResponseOK()
        {
            responseType = this.GetType().Name;
        }
    }
    public class ShareNotExists : ShareResponse
    {
        public ShareNotExists()
        {
            responseType = this.GetType().Name;
        }
    }
    public class PropertyShareNullResponse : ShareResponse
    {
        public PropertyShareNullResponse()
        {
            responseType = this.GetType().Name;
        }
    }
    public class sqlException : ShareResponse
    {
        public string eror;
        public sqlException(string e)
        {
            responseType = this.GetType().Name;
                eror = e;
        }
    }
}
