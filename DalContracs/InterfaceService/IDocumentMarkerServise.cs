﻿using DalContracs.DTO;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Respones.marker;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.InterfaceService
{
    public interface IDocumentMarkerServise
    {
        MarkerResponse CreateMarker(DocumentMarkerRequest docMark);
        MarkerResponse RemoveMarker(DocumentMarkerDetails marker);
        MarkerResponse GetMarker(DocumentMarkerIDRequest marker);
       MarkerResponse GetMarkers(DocumentIDRequest docID);
        MarkerResponse changeMarker(DocumentMarkerDetails docMark);
    }
}
