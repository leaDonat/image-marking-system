﻿using DalContracs.DTO;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentShares;
using DalContracs.Respones.share;
using DalContracs.Respones.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.InterfaceService
{
    public interface ISharedDocumentService
    {
        ShareResponse CreateShare(DocumentShareRequest docShare);
        ShareResponse RemoveShare(DocumentShareRequest docID);
        ShareResponse GetShare(DocumentShareRequest docID);
        ShareResponse GetUsersOfShare(DocumentIDRequest docID);
        ShareResponse GetShares(DocumentShareRequest doc);

    }
}
