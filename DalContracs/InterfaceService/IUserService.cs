﻿using DalContracs.DTO;
using DalContracs.Request.Users;
using DalContracs.Respones.user;
using System;
using System.Collections.Generic;
using System.Text;

namespace DalContracs.InterfaceService
{
    public interface IUserService
    {
        UserResponse GetUser(UserIDRequest userID);

        UserResponse GetUsers();
        UserResponse CreateUSer(UserRequest user);
        UserResponse Login(UserRequest user);
        public UserResponse RemoveUser(UserIDRequest user);
    }
}
