﻿using DalContracs.DTO;
using DalContracs.Request;
using DalContracs.Request.Document;
using DalContracs.Request.Users;
using DalContracs.Respones.document;
using DalContracs.Respones.user;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace DalContracs.InterfaceService
{
    public interface IDocumentService
    {
        DocumentResponse CreateDocument(DocumentRequest doc);
        DocumentResponse CreateCopyFile(DocumentIDRequest docID);
        DocumentResponse GetDocument(DocumentIDRequest docID);
        DocumentResponse GetDocuments();
        DocumentResponse RemoveDocument(DocumentIDRequest docID);
        DocumentResponse GetDocumentsForUser(UserIDRequest userID);


    }
}
