﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracs.InterfaceService;
using DalContracs.Request.Users;
using DalContracs.Respones.user;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDev__.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserService userService;
        public UserController(IUserService user)
        {
            userService = user;
        }

        [HttpPost]
        public UserResponse CreateUser([FromBody] UserRequest user)
        {
            return userService.CreateUSer(user);
        }
        [HttpPost]
        public UserResponse RemoveUser([FromBody] UserIDRequest user)
        {
            return userService.RemoveUser(user);
        }
        [HttpPost]
        public UserResponse Login([FromBody] UserRequest user)
        {
            var e = userService.Login(user);
            return e;
        }

        [HttpGet]
        public UserResponse GetUser([FromBody] UserIDRequest user)
        {
            return userService.GetUser(user);
        }
    }
}
