﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Request.Document;
using DalContracs.Request.Users;
using DalContracs.Respones.document;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDev__.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        IDocumentService docService;
        public DocumentController(IDocumentService doc)
        {
            docService = doc;
        }
        [HttpPost]
        public DocumentResponse CreateDocument([FromBody] DocumentRequest doc)
        {
            return docService.CreateDocument(doc);
        }
        [HttpPost]
        public DocumentResponse CreateCopyFile([FromBody] DocumentIDRequest docID)
        {
            return docService.CreateCopyFile(docID);
        }
        [HttpPost]
        public DocumentResponse RemoveDocument([FromBody] DocumentIDRequest doc)
        {
            return docService.RemoveDocument(doc);
        }
        [HttpGet]
        public DocumentResponse GetDocuments()
        {
            return docService.GetDocuments();
        }
        [HttpGet]
        public DocumentResponse GetDocument([FromBody] DocumentIDRequest doc)
        {
            return docService.GetDocument(doc);
        }
        [HttpPost]
        public DocumentResponse GetDocumentsForUser([FromBody] UserIDRequest userID)
        {
            return docService.GetDocumentsForUser(userID);
        }


    }
}
