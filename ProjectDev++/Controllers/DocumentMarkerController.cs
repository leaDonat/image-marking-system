﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentMarkers;
using DalContracs.Respones.marker;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDev__.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class DocumentMarkerController : ControllerBase
    {
        IDocumentMarkerServise markerService;
        public DocumentMarkerController(IDocumentMarkerServise marker)
        {
            markerService = marker;
        }

        [HttpPost]
        public MarkerResponse CreateMarker([FromBody] DocumentMarkerRequest shareDoc)
        {
            return markerService.CreateMarker(shareDoc);
        }
        [HttpPost]
        public MarkerResponse RemoveMarker([FromBody] DocumentMarkerDetails shareDoc)
        {
            var e= markerService.RemoveMarker(shareDoc);
            return e;
        }
        [HttpPost]
        public MarkerResponse GetMarkers([FromBody] DocumentIDRequest docID)
        {
            return markerService.GetMarkers(docID);
        }
        [HttpPost]
        public MarkerResponse changeMarker([FromBody] DocumentMarkerDetails shareDoc)
        {
            return markerService.changeMarker(shareDoc);
        }
    }
}
