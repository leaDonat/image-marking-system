﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using DalContracs.Request.Document;
using DalContracs.Request.DocumentShares;
using DalContracs.Respones.share;
using DalContracs.WebSocket;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDev__.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class SharedDocumenController : ControllerBase
    {
        ISharedDocumentService shareService;
        IshareWebSocket _webSocket;
        public SharedDocumenController(ISharedDocumentService share, IshareWebSocket webSocket)
        {
            shareService = share;
            _webSocket = webSocket;
        }
        [HttpPost]
        public  ShareResponse CreateShare([FromBody] DocumentShareRequest shareDoc)
        {
            return shareService.CreateShare(shareDoc);
        }
        [HttpPost]
        public ShareResponse RemoveShare([FromBody] DocumentShareRequest shareDoc)
        {
            return shareService.RemoveShare(shareDoc);
        }
        [HttpGet]
        public ShareResponse GetShare([FromBody] DocumentShareRequest shareDoc)
        {
            return shareService.GetShare(shareDoc);
        }
        [HttpGet]
        public ShareResponse GetUsersOfShare([FromBody] DocumentIDRequest shareDoc)
        {
            return shareService.GetUsersOfShare(shareDoc);
        }
        [HttpPost]
        public void CloseWebSocket([FromBody] DocumentShareRequest share)
        {
            _webSocket.CloseWebSocket(share);
        }
    }
}
