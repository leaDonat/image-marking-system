using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;    
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using FactoryDI;
using System.IO;
using System.Net.WebSockets;
using System.Threading;
using DalContracs.WebSocket;
using WebSocketShare;
using DalContracs.Request.DocumentShares;
using InfraDALContracts;
using SQLServerInfraDAL;
using InfraDAL;
using DalContracs.DTO;
using DalContracs.InterfaceService;
using ServiceImpl;
using DalContracs;
using Dal;

namespace ProjectDev__
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IInfraDAL, SQLDAL>();
            services.AddSingleton<IDBConnection, SQLConnectionAdapter>();
            services.AddSingleton<IDBParameter, SqlParameterAdapter>();
            services.AddSingleton<IConvertParameter, ConvertParameter>();
            services.AddTransient<ISocket, WebSocketAdapter>();
            services.AddSingleton<IshareWebSocket, ShareWebSocketAdapter>();
            //services.AddTransient<IUserService, UserService>(); 
            //services.AddTransient<IDocumentMarkerServise, DocumentMarkerServise>();
            //services.AddTransient<IDocumentService, DocumetService>();
            //services.AddTransient<ISharedDocumentService, SharedDocumentService>();
            //services.AddTransient<ISharedDocumentDal, SharedDocumentDal>();
            //services.AddTransient<IDocumentDAl, DocumentDAl>();
            //services.AddTransient<IDocumentMarkerDAl, DocumentMarkerDAL>();
            //services.AddTransient<IUserDal, UserDAL>();
             var resolver = new Resolver("dll", services);
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)   
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWebSockets();
            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                        string userID = context.Request.Query["userID"];
                        Guid docID = new Guid(context.Request.Query["docID"]);
                        var messanger = app.ApplicationServices.GetService<IshareWebSocket>();
                        var webSocketAdapter = new WebSocketAdapter();
                        webSocketAdapter.Socket = webSocket;
                        await messanger.Add(new DocumentShareRequest { userID = userID, docID = docID }, webSocketAdapter);
                        await webSocket.ReceiveAsync(new Memory<byte>(), CancellationToken.None);
                    }

                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
                else
                {
                    await next();
                }
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
