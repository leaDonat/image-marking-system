﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBParameterConverter
{
    public class DBParameterAttribute:Attribute
    {
        public string paramName { get; set; }
        public DBParameterAttribute(string name)
        {
            paramName = name;
        }

    }
}
