﻿using DalParametersConverter;
using InfraDALContracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;

namespace DBParameterConverter
{
    public class DBParameterConverterClass
    {
        IInfraDAL dal;
        static ConcurrentDictionary<Type, Dictionary<string, IGetter>> _getters =
           new ConcurrentDictionary<Type, Dictionary<string, IGetter>>();
        public DBParameterConverterClass(IInfraDAL _dal)
        {
            dal = _dal;
        }
        private Dictionary<string, IGetter> MakeGetters<T>() where T : class
        {
            var retval = new Dictionary<string, IGetter>();
            foreach (var property in typeof(T).GetProperties())
            {
                var attribute = property.GetCustomAttribute<DBParameterAttribute>();
                if (attribute != null)//This property is a parameter 
                {
                    var getter = new Getter<T>(property.Name);
                    retval.Add(property.Name, getter);
                }
            }
            return retval;

        }
        public IDBParameter convertToParameter(object DTO, string parameterName)
        {
            IDBParameter retval = null;
            foreach (var property in DTO.GetType().GetProperties())
            {
                var Attributes = property.GetCustomAttribute<DBParameterAttribute>();
                if (Attributes != null)
                {
                    if (Attributes.paramName.Equals(parameterName))
                    {
                        var paramValue = property.GetValue(DTO);
                        retval = dal.GetParameter(parameterName, paramValue);
                        break;
                    }
                }
            }
            return retval;
        }
        public IDBParameter[] convertToParameters(object DTO)
        {
            List<IDBParameter> retval = new List<IDBParameter>();
            foreach (var property in DTO.GetType().GetProperties())
            {
                var Attributes = property.GetCustomAttribute<DBParameterAttribute>();
                if (Attributes != null)
                {
                    var paramName = Attributes.paramName;
                    var paramValue = property.GetValue(DTO);
                    IDBParameter parameter = dal.GetParameter(paramName, paramValue);
                    retval.Add(parameter);
                }
            }
            return retval.ToArray();
        }
        //Expression 
        public IDBParameter[] ConvertToParameters2<T>(T dto) where T : class
        {
            List<IDBParameter> retval = new List<IDBParameter>();
            if (!_getters.ContainsKey(dto.GetType()))
            {
                var getters = MakeGetters<T>();
                _getters.TryAdd(dto.GetType(), getters);


            }
            foreach (var paramName in _getters[dto.GetType()].Keys)
            {
                var parameter = dal.GetParameter(paramName,
                    _getters[dto.GetType()][paramName].call(dto));
                retval.Add(parameter);
            }
            return retval.ToArray();
        }
    }
}
