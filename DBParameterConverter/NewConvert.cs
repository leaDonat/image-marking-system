﻿using InfraDALContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DBParameterConverter
{
    public class NewConvert
    {
       static IInfraDAL dal;
        public NewConvert(IInfraDAL _dal)
        {
            dal = _dal;
        }
        public List<T> ConvertToLisResponse<T>(DataSet ds)
        {
            var res = new List<T>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in typeof(T).GetProperties())
                    pro.SetValue(objT, item[pro.Name]);

                res.Add(objT);
                //users.Add(new User { userID = Convert.ToString(item["userID"]), userName = Convert.ToString(item["userName"]) });
            }
            return res;
        }
        public T ConvertToResponse<T>(DataSet ds)
        {
            var objT = Activator.CreateInstance<T>();
            DataRow item = ds.Tables[0].Rows[0];
            foreach (var pro in typeof(T).GetProperties())
                pro.SetValue(objT, item[pro.Name]);
            return objT;
        }
        static public IDBParameter[] ConvertToListParametr<T>(T request)
        {
            List<IDBParameter> parameters = new List<IDBParameter>();
            foreach (var pro in typeof(T).GetProperties())
                parameters.Add(dal.GetParameter(pro.Name, pro.GetValue(request)));
                    //{ ParameterName = pro.Name, Value = pro.GetValue(request) });

            return parameters.ToArray();

        }
    }
}
